This is Spencer Apple's and Ken Garman's second project for CMPS 179 at UCSC.
This project uses ThreeJS to visualize a landscape of eBay's 100 oldest book auctions.

CMPS179 is a Data Visualization class taught by Reid Swanson and Matt Maclaurin.

See it in [action](http://splayemu.gitlab.io/CMPS179_EbayVisual2)!

