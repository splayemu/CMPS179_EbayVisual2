//See
//http://developer.ebay.com/Devzone/finding/HowTo/GettingStarted_JS_NV_JSON/GettingStarted_JS_NV_JSON.html

/*****************************************************************************/
/** Define a global variable *************************************************/
/*****************************************************************************/
var EBAY_SEARCH = {
    resultSize: 100
};

var _seconds = 0;

var _items = (function () {
    var my = {},
        count = 0,
        item_list = [],
        first = 0;

    my.createItemData = function (title, bid_count, current_price, time_left, seller_info) {
        item_data = {};

        console.log("Creating item: " + count + " " + title + " with bids " + bid_count + " with " + time_left + " seconds left.");


        item_data["id"] = count;
        item_data["title"]          = title || '';
        item_data["bid_count"]      = bid_count || 0;
        item_data["current_price"]  = current_price || 0;
        item_data["time_left"]      = time_left || '';
        item_data["seller_info"]    = seller_info || {};

        if(item_data.current_price < 1) {
            item_data.current_price = 1;
        }

        var feedback_score = item_data.seller_info.feedbackScore[0];
        item_data.seller_info.feedbackScore[0] = (feedback_score > 0) ? feedback_score : 1;

        //console.log(seller_info);
        item_list.push(item_data);

        _grid.addItem(item_data);
        count++;
    };
/*
    my.hasItemData = function (id) {
        if(getItemData(id) == undefined) {
            return false;
        }
        return true;
    }*/

    my.getItemDataById = function (id) {
        //console.log("getItemDataById: called on " + id);
        for(var i = 0; i < item_list.length; i++) {
            if(item_list[i].id == id) {
                //console.log("getItemDataById: returning " + item_list[i]);
                return item_list[i];
            }
        }
        return undefined;
    };

    my.getItemDataIdByTitle = function (item_title) {
        for(var i = 0; i < item_list.length; i++) {
            if(item_list[i].title === item_title) {
                return item_list[i].id;
            }
        }
        return undefined;
    };

    my.updateItemData = function (item_id, variables) {
        var item_data = this.getItemDataById(item_id);
        var old_price = item_data.current_price;
        var old_bids = item_data.bid_count;
        console.log("Updating item " + item_id + " " + item_data.title + " with " + variables.time_left + " seconds left.");
        //console.log(variables);
        if(old_bids != variables.bid_count) {
            console.log("Updating bid_count from " + old_bids + " to " + variables.bid_count);
        }
        if(old_price != variables.current_price) {
            console.log("Updating current_price from " + old_price + " to " + variables.current_price);
        }
        for(each_variable in variables) {
            //console.log("     " + each_variable + " = " + item_data[each_variable]);
            item_data[each_variable] = variables[each_variable] || item_data[each_variable];
        }
    };

    /* tickItems checks to see if any item has expired */
    my.tickItems = function () {
        for(var i = first; i < item_list.length; i++) {
            item_list[i].time_left--;
            if(item_list[i].time_left <= 0) {
                printItem(item_list[i]);
                console.log("EXPIRED");
                _grid.removeItem(item_list[i].id);
                first++;
            }
        }
    }

    function printItem(item) {
        console.log("Item " + item.id + " " + item.title + "\nExpires in " + item.time_left + "\nPrice " + item.current_price);
    }

    my.printItems = function () {
        item_list.map( printItem(item) );
    };

    return my;

}());

$(document).ready(function () {
    tick();

});

function tick() {
    console.log("Ticking: " + _seconds);
    if(_seconds % 30 == 0) {
        console.log("One minute detected.");
        makeEbayRequest(267);
    }
    _items.tickItems();
    _seconds++;
    setTimeout(tick, 1000);
}



function parseTime(datetime) {
    var datetime_list = datetime.split(/[a-zA-Z]/);
    var days = parseInt(datetime_list[1], 10);
    var hours = parseInt(datetime_list[3], 10);
    var minutes = parseInt(datetime_list[4], 10);
    var seconds = parseInt(datetime_list[5], 10);
    return seconds + (minutes * 60) + (hours * 3600) + (days * 24 * 3600);
    //return days + " days, " + minutes + " minutes, and " + seconds + " seconds.";
}



//var _fail_count = 0;

/**
 * This is the callback referenced in the getFindUrl function defined below.
 * It parses the response from eBay and builds an HTML table to display
 * the search results.
 *
 * @param root
 */
function findItemsByCategory(root) {
    // If no results are found, mark this category as bad!
    /*if (root.findCompletedItemsResponse[0].paginationOutput[0].totalEntries[0] <= 0) {
        console.log("Bad response: Server returned: " + JSON.stringify(root));
        //console.log("findCompletedItems: Remove this category HOLY SHIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIT");
        _fail_count++;
        return;
    } */



    console.log("findItemsByKeywords: JSON received!");
    // get the results or return an empty array if there aren't any.
    var ebay_items = root.findItemsByCategoryResponse[0].searchResult[0].item || [];
   // console.log(ebay_items);

    for (item in ebay_items) {
        //console.log(item);
        var item_title = ebay_items[item].title[0];
        var selling_status = ebay_items[item].sellingStatus[0];
        var seller_info = ebay_items[item].sellerInfo[0];
        //console.log("Bid_string " + selling_status.bidCount[0] + " and bid_int " + parseInt(selling_status.bidCount[0], 10));
        var bid_count = parseInt(selling_status.bidCount[0], 10);
        var current_price = parseFloat(selling_status["convertedCurrentPrice"][0]["__value__"]);
        var time_left = parseTime(selling_status.timeLeft[0]);

        var item_id = _items.getItemDataIdByTitle(item_title);
        if(item_id == undefined) {

            _items.createItemData(item_title, bid_count, current_price, time_left, seller_info);
        }
        else {
            //console.log("Found item: " + item_id);
            _items.updateItemData(item_id, { "bid_count":bid_count, "current_price":current_price, "time_left":time_left });
        }
    }

    // When we're done processing remove the script tag we created below
    var lastChild = document.body.lastChild;
    document.body.removeChild(lastChild);


    //_items.printItems();
}


/**
 * Communicate with the eBay servers using A GET request.
 * A GET request is a base url that is appended by a series of key/value pairs.
 * The base URL is separated from the request by a '?'.
 * Each key/value pair is specified by: key=value.
 * Key/value pairs are separated by an '&'
 *
 * @param query
 * @returns
 */
function getFindUrl(category_id, start_date, end_date) {
    // Base url
    console.log("getFindUrl: Building the query.")

    var url = "http://svcs.ebay.com/services/search/FindingService/v1";

    // GET parameters
    url += "?OPERATION-NAME=findItemsByCategory";
    url += "&SERVICE-VERSION=1.0.0";
    url += "&SECURITY-APPNAME=" + appId;
    url += "&GLOBAL-ID=EBAY-US";
    url += "&RESPONSE-DATA-FORMAT=JSON";

    // When eBay processes the request it will create a javascript object
    // containing the resulting data and wrap the callback function defined
    // below around it. So you need to have a function defined in the script
    // with the name 'findItemsByCategory' (or whatever you call it) that
    // takes one parameter (i.e., the javascript object ebay returns).
   // url += "&outputSelector=CategoryHistogram";
    url += "&outputSelector=SellerInfo";
    url += "&callback=findItemsByCategory";
    url += "&REST-PAYLOAD";
    url += "&categoryId="+category_id;
    url += "&sortOrder=EndTimeSoonest";
    url += "&itemFilter(0).name=ListingType";
    url += "&itemFilter(0).value(0)=AuctionWithBIN";
    url += "&itemFilter(0).value(1)=Auction";
    //url += "&paginationInput.entriesPerPage=" + EBAY_SEARCH.resultSize;

    // Make sure to encode the url to make sure spaces and other special
    // characters are escaped properly.
    // Note: I think this does not handle all cases so be careful.
    // You may need some additional checks here.
    return encodeURI(url);
}
/*
function calculateTodayMinusDays(days) {
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1;
    var yyyy = today.getFullYear();

    for(var i = 0; i < days; i++) {
        dd--;
        if(dd == 0) {
            dd = 30;
            mm--;
            if(mm == 0) {
                mm = 12;
                yyyy--;
            }
            if(mm == 1 || mm == 3 || mm == 5 || mm == 7 || mm == 8 || mm == 10 || mm == 12) {dd = 31;}
        }
    }
    if(dd < 10){dd='0'+dd};
    if(mm < 10){mm='0'+mm};
    return yyyy + "-" + mm + "-" + dd;
}

function queryWeekData(cat_data) {
    cat_data.queries_remaining = 7;

    for(var i = 1; i < 8; i++) {
        var end_date = calculateTodayMinusDays(i);
        var start_date = calculateTodayMinusDays(i + 1);
        console.log("Quering with endDateFrom: " + start_date + " and endDateTo: " + end_date);
        makeEbayRequest(cat_data.id, start_date, end_date);
    }
}*/

/**
 * Actually request the ebay data
 */
function makeEbayRequest(category_id) {
    // Programmatically create a new script tag
    var script = document.createElement('script');

    // Create the URL for requesting data from eBay using a GET request
    // and set the src attribute of the newly created script tag to this URL.
    script.src = getFindUrl(category_id);

    // Add the new tag to the document body which will try to load the script
    // from the URL.
    // eBay will dynamically create a script for us that will load in the
    // attached <script> tag.
    // See the comment in getFindUrl about the callback
    document.body.appendChild(script);
}

/**
 * Returns true if the query is not empty.
 * Note: could be more robust.
 *
 * @param query
 * @returns {Boolean}
 */
function valid(query) {
    return query != '';
}
