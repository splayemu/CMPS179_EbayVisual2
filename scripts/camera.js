var _camera;

//
// Returns the current aspect ratio of the viewport. This is unnecessary
// if we decide to have a static aspect ratio. Ken thinks the aspect ratio
// should be dynamic.
//
function CurrentAspectRatio() {
    if (_viewport.w < 1 && _viewport.h < 1) {
        console.log("Viewport dimensions are askew! Returning 4:3");
        return 4.0/3.0;
    }
    return _viewport.w / _viewport.h;
}

//
// Initializes a ThreeJS PerspectiveCamera and sets its position to (0, 10, 0)
//
function InitCamera() {
    _camera = new THREE.PerspectiveCamera(45, CurrentAspectRatio(), 1, 10000);
    SetCameraPosition(0, 10, 0);
    return _camera;
}

function SetCameraPosition(x, y, z) {
    _camera.position.x = x;
    _camera.position.y = y;
    _camera.position.z = z;
    _camera.lookAt(_scene.position);
}
