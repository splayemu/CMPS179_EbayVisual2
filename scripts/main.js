var _renderer, _scene;
var _viewport = { w: 0, h: 0 };

/*
 * getLinearRate - helper function that calculates a linear rate
 */
function getLinearRate(min, max, num) {

    var rate = 0;
    if(min * max < 0) {

        rate = (Math.abs(min) + Math.abs(max)) / num * ((max > min) ? 1 : -1);

    }
    else {

        rate = (max - min) / num;

    }

    return rate;

}

/*
 * getLogarithmicRate - helper function that calculates a logarithmic rate
 */
function getLogarithmicBase(min, max, num) {
    return Math.pow(max, 1 / num);
}

function FloatingText(text, x, y, z, rx, ry, rz, textSize, cHex)
{
    var my = {};

    my.textGeom = new THREE.TextGeometry(text,
    {
        size: textSize, height: 4, curveSegments: 3,
        font: "helvetiker", weight: "normal", style: "normal",
        bevelThickness: 1, bevelSize: 2, bevelEnabled: true,
        material: 0, extrudeMaterial: 1
    });


    var matFront = new THREE.MeshBasicMaterial({color: cHex});

    my.mesh = new THREE.Mesh(my.textGeom, matFront);

    my.textGeom.computeBoundingBox();
    my.width = my.textGeom.boundingBox.max.x - my.textGeom.boundingBox.min.x;

    my.mesh.position.set(x, y, z);
    my.mesh.rotation.set(rx, ry, rz);

    _scene.add(my.mesh);

    return my;
}

function Plant(material, flower_material, item_data, x, z, size, max_scale) {
    var my = {};

    my.item_data = item_data;
    my.animation = undefined;

    //my.trunk = new THREE.Mesh(new THREE.CylinderGeometry(size, size, size, 10, size),
    my.trunk = new THREE.Mesh(new THREE.CubeGeometry(size, size, size, 10, 10, 10),
    (item_data.bid_count > 0) ? flower_material : material);
    _scene.add(my.trunk);

    my.trunk.position.x = x;
    my.trunk.position.y = size / 2;
    my.trunk.position.z = z;

    my.trunk.scale.x = 0.1;
    my.trunk.scale.y = 0.2;
    my.trunk.scale.z = 0.1;

    var p_max_scale = max_scale;
    var base_plant_width = size;

    // tweens call this method on update
    function updateTrunk() {
        my.trunk.position.y    = my.trunk.scale.y * (base_plant_width / 2);
    }

    // preconditions: item_data is attached to this object
    my.grow = function () {
        var ratio = .5;
        var time_left = my.item_data.time_left * 1000;
        var init_tree_time = 500;

        // sanity check
        if(time_left - init_tree_time < 0) {
            throw new Error("Plant Error: Item " + item.id + " has already expired");
            return;
        }

        var time_to_grow = Math.min(120000, (time_left - init_tree_time) * ratio);
        var time_ending = (time_left - init_tree_time) * (1 - ratio);

        var end_y_scale = Math.min(8 + (1.5 * my.item_data.bid_count), p_max_scale);

        // declare the animation endpoints
        var init_tree_variables        = { x: 1,    z: 1,    y: 1 };
        var growth_tree_variables    = { x: 1.2, z: 1.2, y: end_y_scale / 2 };
        var bloom_tree_variables    = { x: 1.2, z: 1.2, y: end_y_scale  };
        var wither_tree_variables    = { x: 0.2, z: 0.2, y: 0.4 };
        var end_tree_variables = (my.item_data.bid_count > 0) ? bloom_tree_variables : wither_tree_variables;

        // grow the plant to a starting size really quickly (2 seconds)
        var init_tree    = new TWEEN.Tween( my.trunk.scale ).to( init_tree_variables, init_tree_time ).delay(0)
            .onUpdate(function () { updateTrunk() });

        var grow_tree    = new TWEEN.Tween( my.trunk.scale ).to( growth_tree_variables, time_to_grow ).delay(0)
            .onUpdate(function () { updateTrunk() });

        var end_tree     = new TWEEN.Tween( my.trunk.scale ).to( end_tree_variables, time_ending ).delay(0)
            .onUpdate(function () { updateTrunk() })
            .onComplete(function () {
            // should change ending animation to be different
                my.wither(100);
            });

        init_tree.chain(grow_tree);
        grow_tree.chain(end_tree);

        my.animation = init_tree.start();
    }

    my.wither = function (time) {
        var wither_tree_variables  = { x: 0.2, z: 0.2, y: 0.4 };
        my.animation = new TWEEN.Tween( my.trunk.scale ).to( wither_tree_variables, time ).delay(0)
            .onUpdate(function () { updateTrunk(); })
            .onComplete(function () { setTimeout(function () { my.destroy() }, 60000) })
            .start();
    }

    // destroys self and all children
    my.destroy = function () {
        my.animation = undefined;
        my.item_data = undefined;
        _scene.remove(my.trunk);
        my.trunk = undefined;
    }

    return my;
}

var _grid = (function() {
    var my = {},
        plant_material = LambertMaterial( 0xEEEEEE );

    var p_margin = 80,
        p_max_scale = 0,
        p_plant_width = 0,
        p_width = 0,
        p_height = 0,
        p_depth = 0;

    var visible = [],
        item_backup_queue = [],
        max_number = 100;

    var x_log_base = 0,
        x_rate = 0,
        y_rate = 0,
        z_rate = 0;

    var x_axis_label = undefined,
        y_axis_label = undefined,
        x_max_label = undefined,
        y_max_label = undefined,
        xy_min_label = undefined;

    var x_val_min = 0;
    var x_val_max = 1000000;

    var y_val_min = 0;
    var y_val_max = 1000;

    var z_val_min = 0;
    var z_val_max = 500;

    // private functions
    /*
    function calculateColor (val) {
        var hue = (350 - Math.pow(val, 1.55)) / 1000;
        if(hue < 0) hue = 0;
        return "hsb(" + hue + "," + 0.9 + "," + .8 + ")";
    } */


    /*
    function findIndicies() {
        for(var i = 0; i < visible.length; i++) {
            for(var j = 0; j < visible[i].length; j++) {
                if(visible[i][j].item_data == undefined) {
                    //console.log("Found indecies [" + i + "][" + j + "]");
                    return [i, j];
                }
            }
        }
        return undefined;
    }    */


    function createAxisLabels() {
        x_axis_label = FloatingText("Seller Rating", 0, 0, 0, 0, 0, 0, 60, 0x0);
        x_axis_label.mesh.position.set(-x_axis_label.width / 2, -30, 100 + p_height / 2);
        x_axis_label.mesh.rotation.set(-Math.PI / 4, 0, 0);

        z_axis_label = FloatingText("Price", 0, 0, 0, 0, 0, 0, 60, 0x0);
        z_axis_label.mesh.position.set(-600, 0, z_axis_label.width / 2);
        //z_axis_label.mesh.rotation.set(Math.PI / 4, Math.PI / 2, Math.PI / 4);
        z_axis_label.mesh.rotation.set( -Math.PI / 2, 0, Math.PI / 2 );
        //z_axis_label.mesh.rotation.set(-Math.PI / 4, 0, 0);

        x_max_label = FloatingText(JSON.stringify(x_val_max), 380, 0, 530, -Math.PI / 4, 0, 0, 30, 0x0);
        z_max_label = FloatingText(JSON.stringify(z_val_max), -550, 0, -500, -Math.PI / 4, 0, 0, 30, 0x0);
        xz_min_label = FloatingText("0", -p_width / 2, 0, (p_height / 2) + 30, -Math.PI / 4, 0, 0, 30, 0x0);
    }

    function createItemPlant (item_data) {
        console
        if(visible.length >= max_number) {
            throw new Error("Grid Error: the visible is full.");
            return;
        }
        if(item_data.time_left < 3) {
            //throw new Error("Grid Error: the time left of the auction is too low.");
            console.log("*************************Grid Error: the time left of the auction is too low.*************************");
            return;
        }
        //var current_price = item_data.current_price;

        var feedback_rating_star = item_data.seller_info.feedbackRatingStar[0];
        var feedback_score = item_data.seller_info.feedbackScore[0];

        var flower_material = my.star_mapping[feedback_rating_star];


        console.log("Adding " + item_data.title + " with current price " + item_data.current_price + " and score " + feedback_score + " in the " + visible.length + " position.");
        // create a plant
        // linear var x_pos = -(p_width / 2) +  feedback_score / x_rate;
        var x_pos = -(p_width / 2) + Math.log(feedback_score) / Math.log(x_log_base);
        // linear var z_pos  -(p_height / 2) + item_data.current_price / z_rate;
        var z_pos = +(p_height / 2) - Math.log(item_data.current_price) / Math.log(z_log_base);

        //console.log("With x_rate " + x_rate + " * " + feedback_score + " = " + x_rate * feedback_score);
        //console.log("With z_rate " + z_rate + " * " + item_data.current_price + " = " + z_rate * item_data.current_price);

        var new_plant = new Plant(plant_material, flower_material, item_data, x_pos, z_pos, p_plant_width, p_max_scale);
        new_plant.grow();

        visible.push(new_plant);
    }

    // public functions
    my.init = function (scene, x, y, z, width, height, depth, plant_width) {

        my.x = x;
        my.y = y;
        my.z = z;

        p_width = width;
        p_height = height;
        p_depth = depth;

        p_plant_width = plant_width;
        p_max_scale = depth / plant_width;
        console.log("Grid's max scale is: " + p_max_scale);


        x_log_base = Math.pow(x_val_max, 1 / width);
        z_log_base = Math.pow(z_val_max, 1 / height);

        // axis borders
        var axis_size = 1;

        var x_axis = new THREE.Mesh(new THREE.CubeGeometry(p_width * axis_size, 2, 2, 10, 10, 10), new THREE.MeshBasicMaterial({color: 0x000000}));
        x_axis.position.set(p_width * (axis_size / 2 - .5) - (plant_width / 2), 1, p_height / 2 + (plant_width / 2));

        var y_axis = new THREE.Mesh(new THREE.CubeGeometry(2, p_depth * axis_size, 2, 10, 10, 10), new THREE.MeshBasicMaterial({color: 0x000000}));
        y_axis.position.set(-p_width / 2 - (plant_width / 2), p_depth * (axis_size / 2), p_height / 2 + (plant_width / 2));

        var z_axis = new THREE.Mesh(new THREE.CubeGeometry(2, 2, p_height * axis_size, 10, 10, 10), new THREE.MeshBasicMaterial({color: 0x000000}));
        z_axis.position.set(-p_width / 2 - (plant_width / 2), 1, p_height * -(axis_size / 2 - .5) + (plant_width / 2));

        _scene.add(x_axis);
        _scene.add(y_axis);
        _scene.add(z_axis);
        /*

            Things to consider... perhaps score is better?
                Ranges from 4-10000 ? log of it on one axis?

            Stars are just groupings already
            None
            Yellow star                = 10 to 49 ratings
            Blue star                = 50 to 99 ratings
            Turquoise star            = 100 to 499 ratings
            Purple star                = 500 to 999 ratings
            Red star                = 1,000 to 4,999 ratings
            Green star                = 5,000 to 9,999 ratings
            Yellow shooting star    = 10,000 to 24,999 ratings
            Turquoise shooting star    = 25,000 to 49,999 ratings
            Purple shooting star    = 50,000 to 99,999 ratings
            Red shooting star        = 100,000 to 499,999 ratings
            Green shooting star        = 500,000 to 999,999 ratings
            Silver shooting star    = 1,000,000 ratings or more

            12 rows

        */
        my.star_mapping = {};

        my.star_mapping["None"]                 = LambertMaterial( 0xCE0071 );
        my.star_mapping["Yellow"]                 = LambertMaterial( 0xAC1978 );
        my.star_mapping["Blue"]                 = LambertMaterial( 0x8A317F );
        my.star_mapping["Turquoise"]             = LambertMaterial( 0x684A86 );
        my.star_mapping["Purple"]                 = LambertMaterial( 0x45628C );
        my.star_mapping["Red"]                     = LambertMaterial( 0x237B93 );
        my.star_mapping["Green"]                 = LambertMaterial( 0x01939A );
        my.star_mapping["YellowShooting"]         = LambertMaterial( 0x2B9E80 );
        my.star_mapping["TurquoiseShooting"]     = LambertMaterial( 0x56A967 );
        my.star_mapping["PurpleShooting"]         = LambertMaterial( 0x80B44D );
        my.star_mapping["RedShooting"]             = LambertMaterial( 0xAABF33 );
        my.star_mapping["GreenShooting"]         = LambertMaterial( 0xD5CA1A );
        my.star_mapping["SilverShooting"]         = LambertMaterial( 0xFFD500 );

        createAxisLabels();

    }

    my.addItem = function (item_data) {
        if(visible.length < max_number) {
            createItemPlant(item_data);
        }
        else {
            //console.log("Pushing item" + item_data.id + " to the backup queue[" + item_backup_queue.length + "].");
            item_backup_queue.push(item_data);
        }
    }

    my.updateItems = function () {
        //console.log("item_backup_queue.length " + item_backup_queue.length + " visible.length " + visible.length + " and max_number " + max_number);
        // First adds any plants to the visible if there is space
        while(item_backup_queue.length > 0 && visible.length < max_number) {
            //console.log("Trying to add an item to the visible");
            createItemPlant(item_backup_queue.shift());
        }
    }

    my.removeItem = function (id) {
        for(var i = 0; i < visible.length; i++) {
            var current_tree = visible[i];
            if(current_tree.item_data != undefined && current_tree.item_data.id == id) {

                //console.log("Removing item_id " + current_tree.item_data.id + " at position" + i);
                //current_tree.item_data = undefined;

                // destroy after a minute
                //setTimeout(function () { current_tree.destroy(); }, 60000);

                // remove the item from the array
                visible.splice(i, 1);

                // trigger aggregate_visual
                _aggregate_visual.addItem(current_tree.item_data);
            }
        }
    }

    return my;
}());

$(document).ready(InitPage);

function DegreesToRadians(deg) {
    return deg * (Math.PI/180);
}

//
// Initializes the renderer and appends it to its place in the HTML
//
function InitRenderer() {
    var renderer = new THREE.WebGLRenderer({antialias: true});
    renderer.setClearColor(0x0, 0.0);
    $('#threeDiv').append(renderer.domElement);
    renderer.shadowMapEnabled = true;
    renderer.shadowMapCullFrontFaces = false;
    return renderer;
}

//
// Initializes a ThreeJS Scene
//
function InitScene() {
    return new THREE.Scene();
}

var controls;

function InitPage() {

    var WIDTH = $('#threeDiv').width(),
      HEIGHT = $(window).height() - $('#TitleDiv').height() * 2;

    var VIEW_ANGLE = 45,
      ASPECT = WIDTH / HEIGHT,
      NEAR = 0.1,
      FAR = 10000;

    _renderer = InitRenderer();

    var camera =
      new THREE.PerspectiveCamera( VIEW_ANGLE, ASPECT, NEAR, FAR );

    controls = new THREE.OrbitControls( camera, document );
    controls.addEventListener( 'change', render );
    controls.userPan = false;

    _scene = InitScene();

    // the camera starts at 0,0,0
    // so pull it back
    camera.position.set(0,700,1200);

    // Set the center of rotation for the camera
    controls.center = _scene.position;

    _renderer.setSize(WIDTH, HEIGHT);
    UpdateCanvasSize();

    var floor_material = LambertMaterial( 0xB89470 );
    floor_material.side = THREE.DoubleSide;
    var floor_geometry = new THREE.PlaneGeometry(1000, 1000, 10, 10);
    var floor = new THREE.Mesh(floor_geometry, floor_material);

    floor.position.x = 0 - 5;
    floor.position.z = 0 + 5;
    floor.rotation.x = -Math.PI / 2;
    _scene.add(floor);

    _grid.init(_scene, 0, 0, 0, 1000, 1000, 500, 10);

    function createPointLight(x, y, z, hColor) {
        var light = new THREE.PointLight( hColor );
        light.position.set( x, y, z );
        _scene.add(light);
    }

    var pointLightColor = 0x888888;

    createPointLight( 0, 500, -750, pointLightColor );
    createPointLight( 0, 500, 750, pointLightColor );
    createPointLight( 750, 500, 0, pointLightColor );
    createPointLight( -750, 500, 0, pointLightColor );

    var number_of_k = 0;

    function animate(t) {

        if(t > (number_of_k + 1) * 1000) {
            number_of_k++
            _grid.updateItems();
        }

        // renderer automatically clears unless autoClear = false
        controls.update();
        render();
        TWEEN.update();
        window.requestAnimationFrame(animate, _renderer.domElement);
    }

    function render() {
        _renderer.render(_scene, camera);
    }

    animate(new Date().getTime());
}

function RegisterCallbacks() {
    $(window).on('resize', UpdateCanvasSize);
}

//
// Grabs the browser's viewport dimensions. Since $(window).height() and .width()
// return the browser's dimensions, they have to be calculated oddly. This is a HACK:
// do not erase
//
function UpdateCanvasSize() {
    _viewport.w = $('#threeDiv').width();
    _viewport.h = $(window).height() - $('#TitleDiv').height() * 2;
    _renderer.setSize(_viewport.w, _viewport.h);
}

//
// Returns a new THREE.MeshLambertMaterial object with the specified color
//
function LambertMaterial(cHex) {
    return new THREE.MeshLambertMaterial({color: cHex});
}
