$(document).ready(init);


var feedback_stars = [];
    feedback_stars.push( {name: "None",                 color: "#CE0071", sold: 0, total: 0, description: "9 ratings or less"} );
    feedback_stars.push( {name: "Yellow",                 color: "#AC1978", sold: 0, total: 0, description: "10 to 49 ratings" } );
    feedback_stars.push( {name: "Blue",                 color: "#8A317F", sold: 0, total: 0, description: "50 to 99 ratings" } );
    feedback_stars.push( {name: "Turquoise",             color: "#684A86", sold: 0, total: 0, description: "100 to 499 ratings" } );
    feedback_stars.push( {name: "Purple",                 color: "#45628C", sold: 0, total: 0, description: "500 to 999 ratings" } );
    feedback_stars.push( {name: "Red",                     color: "#237B93", sold: 0, total: 0, description: "1,000 to 4,999 ratings" } );
    feedback_stars.push( {name: "Green",                 color: "#01939A", sold: 0, total: 0, description: "5,000 to 9,999 ratings" } );
    feedback_stars.push( {name: "YellowShooting",         color: "#2B9E80", sold: 0, total: 0, description: "10,000 to 24,999 ratings" } );
    feedback_stars.push( {name: "TurquoiseShooting",    color: "#56A967", sold: 0, total: 0, description: "25,000 to 49,999 ratings" } );
    feedback_stars.push( {name: "PurpleShooting",        color: "#80B44D", sold: 0, total: 0, description: "50,000 to 99,999 ratings" } );
    feedback_stars.push( {name: "RedShooting",            color: "#AABF33", sold: 0, total: 0, description: "100,000 to 499,999 ratings" } );
    feedback_stars.push( {name: "GreenShooting",        color: "#D5CA1A", sold: 0, total: 0, description: "500,000 to 999,999 ratings" } );
    feedback_stars.push( {name: "SilverShooting",        color: "#FFD500", sold: 0, total: 0, description: "1,000,000 ratings or more" } );

var _aggregate_visual = (function () {
    var my = {};

    my.title_label = "Percent of Auctions Sold by Seller Rating";
    my.x_label = "Different Feedback Ratings";
    my.y_label = "Percent of Auctions Sold";

    var outer_graph_border = undefined;
    var inner_graph_border = undefined;
    var bars = [];


    function updateGraph() {
        //var max = find_max_feedback_star_count();
        var y_difference = my.y_axis.y_difference;
        console.log("updateGraph max " + my.y_axis.max + " y_difference " + y_difference);
        for(var i = 0; i < feedback_stars.length; i++) {
            var rectangle_height = (feedback_stars[i].sold > 0) ? my.y_axis.height * (feedback_stars[i].sold / feedback_stars[i].total) : 0.01;
            //console.log("Height calculated is: " + y_difference * feedback_stars[i].count);
            bars[i].attr( {"height": rectangle_height, "y": my.y_axis.y + my.y_axis.height - rectangle_height} ).toFront();
        }

    }

    function find_feedback_star(name) {

        for(var i = 0; i < feedback_stars.length; i++) {
            //console.log("Comparing feedback_star.name " + feedback_stars[i].name + " and name " + name);
            if(feedback_stars[i].name == name) {
                //console.log("Found the item I am looking for");
                return feedback_stars[i];
            }
        }
        return undefined;
    }

    function find_max_feedback_star_count() {
        var max = 0;
        for(var i = 0; i < feedback_stars.length; i++) {
            var ratio = feedback_stars[i].sold / feedback_stars[i].total;
            if( ratio > max) {
                max = ratio;
            }
        }
        return max;
    }

    function createYAxis(min, max, num) {
        var y_axis = {};
        y_axis.x = my.x + my.x_margin;
        y_axis.y = my.y + my.y_margin;
        y_axis.ticks = [];
        y_axis.labels = [];
        y_axis.max = max;
        y_axis.min = min;
        y_axis.num = num;
        y_axis.height = my.height - (2 * my.y_margin);

        function drawHorizontalBar(y) {
            return my.paper.path("M" + (my.x + my.x_margin) + " " + y + "L" + (my.x + my.width - my.x_margin) + " " + y);
        }

        function drawLabel(y, label) {
            return my.paper.text(my.x + my.width - my.x_margin / 2, y, label);
        }

        var rate = getLinearRate(min, max, num);
        var y_difference = (my.height - (2 * my.y_margin)) / num;
        y_axis.y_difference = y_difference;

        for(var i = 0; i <= num; i++) {
            y_axis.ticks.push(drawHorizontalBar(y_axis.y + y_difference * i));
            y_axis.labels.push(drawLabel(y_axis.y + y_difference * i, (max - rate * i).toFixed(2) )); //(max - rate * i).toFixed(2)
        }

        y_axis.destroy = function () {
            for(var i = 0; i <= num; i++) {
                y_axis.ticks[i].remove();
                y_axis.labels[i].remove();
            }
            y_axis.x = undefined;
            y_axis.y = undefined;
            y_axis.ticks = undefined;
            y_axis.labels = undefined;
        }

        //y_axis.line = my.paper.path("M" + y_axis.x + " " + y_axis.y + "L" + y_axis.x + " " + (my.y + my.height - my.y_margin));
        return y_axis;
    }


    my.init = function(paper, x, y, width, height, y_min, y_max) {
        my.paper = paper;
        my.x = x;
        my.y = y;
        my.width = width;
        my.height = height;
        my.y_min = y_min;
        my.y_max = y_max;

    //    outer_graph_border = paper.rect(x, y, width, height).attr({"fill": "white"});
    //    inner_graph_border = paper.rect
        my.x_axis_label = paper.text(x + width / 2, y + height - 15, my.x_label).attr({"font-size": 14});
        my.y_axis_label = paper.text(x + 10, y + height / 2, my.y_label).attr({"font-size": 14}).transform("r270");
        my.title         = paper.text(x + width / 2, y, my.title_label).attr({"font-size": 20});
        my.x_margin = 25;
        my.y_margin = 25;

        my.y_axis = createYAxis(0, 100, 10);

        var x_axis_start = x + my.y_margin;
        var space_between_rectangles = 5;
        var rectangle_size = (width - (2 * my.x_margin) - (feedback_stars.length * space_between_rectangles)) / feedback_stars.length;
        for(var i = 0; i < feedback_stars.length; i++) {
            bars[i] = paper.rect(x_axis_start + (rectangle_size + space_between_rectangles) * i, y + height - my.y_margin - rectangle_size, rectangle_size, 0)
                                .attr({"fill": feedback_stars[i].color });
        }

    }


    my.addItem = function(item_data) {
        // find row index
        var feedback_rating_star = item_data.seller_info.feedbackRatingStar[0];
        //var feedback_score = item_data.seller_info.feedbackScore[0];
        var feedback_star = find_feedback_star(feedback_rating_star);
        // increment rectangle at index
        if(item_data.bid_count > 0) {
            feedback_star.sold++;
        }
        feedback_star.total++;

        /*var feedback_star_max_count = find_max_feedback_star_count();
        // if y_axis_max / max_rectangle > .8,
        console.log("Y_axis_max " + (my.y_axis.max / 100) + " - feedback_max_count " + feedback_star_max_count + " = " + (my.y_axis.max / 100 - feedback_star_max_count));
        if(Math.abs((my.y_axis.max / 100) - feedback_star_max_count) > 0.2) {
            // resize y_axis_max *= 1.5
            var min = my.y_axis.min;
            var num = my.y_axis.num;
            var max = Math.max(100, 100 * (feedback_star_max_count * 1.25));
            my.y_axis.destroy();
            my.y_axis = createYAxis(min, max, num);
        } */
        // adjust all rectangle heights
        updateGraph();
    }

    function update(size) {



    }


    my.redrawGraph = function(time) {

    }


    return my;
}());

function init() {
    var width     = document.getElementById("raphDiv").offsetWidth;
    var height    = document.getElementById("raphDiv").offsetHeight;

    console.log("Size is w " + width + " h " + height);

    var rapheal_width = width;
    var rapheal_height = height;

    var rapheal_x_pos = 0;//width - rapheal_width;
    var rapheal_y_pos = 0;

    var graph_width = rapheal_width - 30;
    var graph_height = 280;

    var graph_x_pos = rapheal_x_pos + 25;
    var graph_y_pos = rapheal_y_pos + 25;

    var paper = Raphael("raphDiv", rapheal_width, rapheal_height);

    console.log("Initializing the ongoing visual at x " + graph_x_pos + " y " + graph_y_pos + " with width " + graph_width  + " and height " + graph_height);

    //paper.rect(10, 10, 300, 300).attr({"fill":"white"});

    // (x, y, width, height, y_min, y_max)
    _aggregate_visual.init(paper, graph_x_pos, graph_y_pos, graph_width, graph_height, 0, 5);

    var space_between_graph_and_legend = 90;
    var feedback_title1 = paper.text(graph_x_pos, graph_y_pos + graph_height + 15, "Buyers leave feedback on Sellers.")
        .attr({"font-size": 12, 'text-anchor': 'start'});
    var feedback_title2 = paper.text(graph_x_pos, graph_y_pos + graph_height + 30, "The number of ratings is the sum of all of the reviews.")
        .attr({"font-size": 12, 'text-anchor': 'start'});
    var feedback_descripton_1 = paper.text(graph_x_pos + 5, graph_y_pos + graph_height + 45, "+1 point for each positive rating")
        .attr({"font-size": 12, 'text-anchor': 'start'});
    var feedback_descripton_2 = paper.text(graph_x_pos + 5, graph_y_pos + graph_height + 60, "No points for each neutral rating")
        .attr({"font-size": 12, 'text-anchor': 'start'});
    var feedback_descripton_3 = paper.text(graph_x_pos + 5, graph_y_pos + graph_height + 75, "-1 point for each negative rating")
        .attr({"font-size": 12, 'text-anchor': 'start'});
    // draw legend
    //var legend_border = paper.rect(graph_x_pos, graph_y_pos + graph_height + space_between_graph_and_legend, graph_width, 200);

    var space_for_legend_items = 20;

    for(var i = 0; i < feedback_stars.length; i++) {
        var x_pos = (i < feedback_stars.length / 2) ? graph_x_pos : graph_x_pos + 150;
        var y_pos = graph_y_pos + graph_height + space_between_graph_and_legend + (i % (feedback_stars.length / 2)) * space_for_legend_items;

        paper.rect(x_pos, y_pos, 10, 10).attr({"fill": feedback_stars[i].color });
        paper.text(x_pos + 15, y_pos + 5, feedback_stars[i].description).attr({"font-size": 12, 'text-anchor': 'start'});
    }



}
